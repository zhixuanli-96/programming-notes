#include <iostream>
#include <cmath>

class Vector
{
public:
  // create a vector of dimension m
  Vector(int m) : n(m) {
    a = new double[n];
  }
  ~Vector() {
    delete[] a; // free the allocated memory
    a = 0;
  }
  int numElements() const { return n; }

  // read-only access to the i-th element
  const double &operator[] (int i) const {
    return a[i];
  }
  
  // access the i-th element, non-const version
  double &operator[] (int i) {
    return a[i];
  }

  double twoNorm() const {
    double r = 0;
    for(int i=0; i<n; i++)
      r += a[i] * a[i];
    return sqrt(r);
  }

private:
  double *a;
  int n;
};
// define how to print a vector
std::ostream &operator<<(std::ostream &os, const Vector &v)
{
  os << "(";
  int n = v.numElements();
  for(int i=0; i<n-1; i++)
    os << v[i] << ", ";
  os << v[n-1] << ")";
  return os;
}

int main()
{
  Vector v(3);
  v[0] = 1; v[1] = 2; v[2] = -1;
  std::cout << "|" << v << "| = " << v.twoNorm() << std::endl;
  return 0;
}
