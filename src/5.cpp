#include <iostream>
using std::cout;
using std::endl;

template <int n>
struct ct_factorial {
  enum { value = ct_factorial<n-1>::value * n };
};

// specialization of n=0
template <>
struct ct_factorial<0> {
  enum { value = 1 };
};

int main()
{
  cout << "3! = " << ct_factorial<3>::value << endl;
  cout << "4! = " << ct_factorial<4>::value << endl;
  cout << "5! = " << ct_factorial<5>::value << endl;
  cout << "10! = " << ct_factorial<10>::value << endl;
  return 0;
}
