#include <iostream>
#include <complex>
using std::cout;
using std::endl;
using std::complex;

// represent ax + b = 0
template <class T>
class LinearEquation
{
private:
  T a, b;

public:
  LinearEquation(T _a, T _b) : a(_a), b(_b) { }
  T root() const { return -b/a; }
};

int main()
{
  LinearEquation<double> e(2.0, -1.0);
  cout << "Root of 2x - 1 = 0 is " << e.root() << endl;

  typedef complex<double> myComplex;
  LinearEquation<myComplex> f(myComplex(1,2), 1);
  cout << "Root of (1+2i)x + 1 = 0 is " << f.root() << endl;

  return 0;
}
