#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <map>
#include <utility>
#include <cstdlib>

using std::vector;
using std::string;
using std::map;
using std::cout;
using std::endl;

const char *courseNames[] =
  {
   "mathematical analysis",
   "advanced algebra",
   "abstract algebra",
   "algebraic topology"
  };
const int numCourses = sizeof(courseNames) / sizeof(courseNames[0]);

int main()
{
  vector<string> courses;
  map<string,int> plan;
  for(int i=0; i<numCourses; i++) {
    string name(courseNames[i]);
    courses.push_back(name);
    plan.insert(std::make_pair(name, rand()%4+1));
  }
  // traversing a vector using []
  cout << "Courses available : " << endl;
  for(int i=0; i<courses.size(); i++)
    cout << courses[i] << ", ";
  cout << endl << endl;

  // traversing a map using iterators
  for(map<string,int>::iterator it=plan.begin();
      it!=plan.end();
      ++it) {
    cout << "You should learn " << it->first << " in year "
	 << it->second << "." << endl;
  }

  // queries
  const char *anotherCourse = "real analysis";
  cout << endl
       << "Is " << anotherCourse << " available : "
       << std::boolalpha << (plan.find(anotherCourse) != plan.end()) << endl;
  
  return 0;
}
