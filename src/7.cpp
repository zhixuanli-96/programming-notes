#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using std::string;
using std::vector;
using std::cout;
using std::endl;

struct Student
{
  string name;
  int score;
  Student(const string &_n, int _s) : name(_n), score(_s) { }
};

// user-prescribed partial order
bool Student_less_than(const Student &lhs, const Student &rhs)
{
  if(lhs.score > rhs.score) return true;
  if(lhs.score == rhs.score && lhs.name < rhs.name) return true;
  return false;
}

int main()
{
  vector<Student> v;
  v.push_back(Student("Amy",60));
  v.push_back(Student("Bob",90));
  v.push_back(Student("Kat",61));
  v.push_back(Student("Alice",60));
  std::sort(v.begin(), v.end(), Student_less_than); // <-

  for(vector<Student>::iterator it=v.begin();
      it!=v.end(); ++it) {
    cout << it->name << " " << it->score << endl;
  }

  return 0;
}
