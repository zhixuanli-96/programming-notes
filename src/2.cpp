#include <iostream>
#include <cmath>
using std::cout;
using std::endl;

class Shape
{
public:
  virtual double area() const = 0;
  virtual ~Shape() { cout << "~Shape()" << endl; }
};

class Circle : public Shape
{
public:
  Circle(double r) : radius(r) { }
  ~Circle() { cout << "~Circle()" << endl; }
  double area() const { return M_PI * radius * radius; }
protected:
  double radius;
};

class Rectangle : public Shape
{
public:
  Rectangle(double _a, double _b) : a(_a), b(_b) { }
  virtual ~Rectangle() { cout << "~Rectangle()" << endl; }
  double area() const { return a*b; }
protected:
  double a, b;
};

class Square : public Rectangle
{
public:
  Square(double _a) : Rectangle(_a, _a) { }
  ~Square() { cout << "~Square()" << endl; }
};

int main()
{
  double sqr2 = sqrt(2);
  Shape *p[2] = { new Circle(1), new Rectangle(sqr2,1) };
  Rectangle *q = new Square(sqr2);

  cout << "Area of circle = " << p[0]->area() << endl
       << "Area of rectangle = " << p[1]->area() << endl
       << "Area of square = " << q->area() << endl << endl;

  delete p[0]; delete p[1]; delete q;
  return 0;
}
