#include <iostream>
#include <iomanip>
#include <cmath>
using std::cout;
using std::endl;

const double eps = 1e-10;

class Ellipse
{
public:
  Ellipse(double x, double y, double s1, double s2)
    : xc(x), yc(y), r1(s1), r2(s2)
  { }

  double area() const { return M_PI*r1*r2; }

protected:
  double xc, yc, r1, r2;
};

class Circle : public Ellipse
{
public:
  Circle(double x, double y, double r)
    : Ellipse(x,y,r,r)
  { }

  // tangent to the line Ax+By+C ?
  bool tangentTo(double A, double B, double C) const {
    double dist = fabs(A*xc + B*yc + C);
    dist /= sqrt(A*A + B*B);
    return (fabs(dist-r1) < eps);
  }
};

int main()
{
  double sqr2 = sqrt(2);
  Circle c(1, 1, sqr2);
  
  cout << "Area = " << c.area() << endl;
  cout << "Is tangent to x+y=0 : " << std::boolalpha
       << c.tangentTo(1, 1, 0) << endl;
  cout << "Is tangent to y=0 : "
       << c.tangentTo(0, 1, 0) << endl;
  return 0;
}
