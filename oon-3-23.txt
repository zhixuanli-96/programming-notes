
oon-digest          Thursday, March 23 2000          Volume 01 : Number 057



* In this issue:
OON: Comparison between Fortran & C++
RE: OON: Comparison between Fortran & C++
Re: OON: Comparison between Fortran & C++
Re: OON: Comparison between Fortran & C++
RE: OON: Comparison between Fortran & C++
Re: OON: Comparison between Fortran & C++
RE: OON: Comparison between Fortran & C++

Re: OON: Comparison between Fortran & C++
Re: OON: Comparison between Fortran & C++
RE: OON: Comparison between Fortran & C++
Re: OON: Aliasing (was: Comparison between Fortran & C++)

----------------------------------------------------------------------

Date: Wed, 22 Mar 2000 09:40:13 -0700 (MST)
From: Steve Karmesin <karmesin@lanl.gov>
Subject: OON: Comparison between Fortran & C++

This is the holy war of holy wars.  Let me try and start it off
rationally.

I've spent the last few years working on the POOMA framework, using
every trick in the book -- and many that aren't in any books -- to
build general purpose scientific class libraries for high performance
and parallelism.  We're using expression templates, lazy evaluation,
cache optimizations, loop reorderings, the whole nine yards. My team
and I have worked with other folks, like Todd Veldhuizen of Blitz++
and Jeremy Siek of MTL, and while I obviously can't speak for them,
the opinions below reflect conversations with them and I'm pretty
confident of them.

The basic deal is that the languages are different.  They were
conceived with different goals in mind and have different capabilities
as a result.  Modern C++ can do a lot of the things that a decade ago
only made sense in FORTRAN, and modern FORTRAN can do some things that
decade ago didn't make any sense at all in FORTRAN.

There are many syntax differences.  FORTRAN has many ugly aspects, and
C++ (particularly with templates) has its warts.  To my eye C++ has
fewer ugly syntax things and is more convenient to do complex things
in.  To some degree that is a matter of taste and the task you have to
do.

There are more substantive differences.  Given the forum I'll hit the
things FORTRAN does well first since I think those are less well
understood.

If the only data structure you need is flat multidimensional arrays,
FORTRAN does a very good job out of the box.  With FORTRAN 90 you get
array syntax (i.e. you can say A=B+C*D for whole arrays), which is
very convenient and very difficult to do well in C++ (trust me on
this).  It is easy to match FORTRAN 77 arrays in C++, but FORTRAN 90
arrays are difficult to match.  Not completely impossible, but it
really is a Don't Try This At Home Kids(tm) kind of thing.  At this
point though you can pick up POOMA, Blitz or the MTL to do this stuff
for you in C++, depending on the mix of features you're looking for.

There is a semantic difference in passing arrays to subroutines in
FORTRAN compared to C++: In FORTRAN you are guaranteeing that they are
not aliased.  This can a really big deal for the optimizer when doing
numeric intensive work, and is the primary remaining factor that makes
many FORTRAN programs faster than the equivalent C++ programs.  There
are proposals to improve this situation in C/C++ (primarily the
restrict keyword at this point), but they are spottily implemented.
The alternative to something like restrict is a godlike global
optimizer that can figure out the aliasing, but that is way in the
future for other than toy problems at this point. The bottom line,
sadly enough, is that given comparably smart optimizers, under
circumstances that are pretty common in scientific computing, the
C/C++ equivalent to a FORTRAN program, will often not run as fast.
Depending on the architecture it may not be a big difference, but it
can be up to a factor of two on conventional workstations, and much
larger than that on vector machines.

That is something FORTRAN does well, and we need to be rational about
it and not be bigots.

If we grant that modern FORTRAN does arrays better, we then have to go
to the things that C++ does better.
C++ is vastly easier to use for making exensible libraries of all
kinds.  That includes inheritance heirarchies (which you really can't
do in F90) and generic libraries (templates, which you can't do at all
in F90).  This is a really big deal, particularly for large projects.
You can't do anything like the STL in FORTRAN.

With F77, if it does what you need out of the box, you're golden.  If
you need 5% more, you're dead.  There is a cliff at the edge of arrays
and it is a long way down.

With F90, if you need more, then in some cases you can put together
the data structures to do it.  Those data structures are nowhere near
as flexible or powerful as in C++, but you have more tools available
than with F77, and the arrays are convenient out of the box.

C++ is a Swiss Army Knife(tm) with a vast number of tools in it.  By
picking the appropriate tools you can do everything from device
drivers to databases to nuclear weapon simulations.  It is also true
that if you pick inappropriate tools you will cut your hands off
before you know what happened.  And there are so many tools and so
many completely different programming styles possible in C++ that it
is difficult to understand the tools well enough to keep from cutting
yourself.

Personally, I'll take that risk instead of the risk of there not being 
any feasible way to do what I want, which is more likely for many many 
problems with a language like FORTRAN.

- -Steve Karmesin

p.s. Please folks, no jihads.

Cesare Padovani writes:
 > Could someone tell me where I can find a comparison between Fortran & C++.
 > 
 > Best regards. Cesare Padovani.
------------------------------

Date: Thu, 23 Mar 2000 09:00:34 -0800 (PST)
From: Geoffrey Furnish <furnish@actel.com>
Subject: RE: OON: Comparison between Fortran & C++

One tiny point of clarification:

Steve Karmesin writes:
 > There is a semantic difference in passing arrays to subroutines in
 > FORTRAN compared to C++: In FORTRAN you are guaranteeing that they are
 > not aliased.  

Please someone correct me if what I say next is wrong, but I don't
think this is really the right way to say it.  My understanding is
that the language /assumes/ array parameters to subroutines are
unaliased, and allows the compiler to optimize accordingly.  It is
actually a fairly common practice in FORTRAN to do purposeful aliasing
in which you effectively redimension an array across a subroutine call
site.  For example, you may have a 3 dimensional array in the caller,
and pass one element of it to the subroutine, which receives it as a
2-d array, thus taking a slice.  Sometimes people actually do this
stunt with overlapping regions, and think they'll be safe because they
know how the subroutine walks the data.  But I have a good friend who
lost about 2 weeks once, tracking down a bug that turned out to be a
case where Cray FORTRAN made very effective use of the presumption of
nonaliasing, and was able to reorder the subroutine's code so that it
would be faster, but visited the elements in a manner that was
different than he expected, and so the computed output was tragically
wrong, as a result of "lieing" about the non aliasing.  In other
words, FORTRAN doesn't guarantee that arrays are unaliased, it assumes
it.  You'd better make sure you don't violate that assumption, and yet
htere are common programming styles which do so routinely.

- -- 
Geoffrey Furnish            Actel Corporation        furnish@actel.com
Senior Staff Engineer      955 East Arques Ave       voice: 408-522-7528
Placement & Routing     Sunnyvale, CA   94086-4533   fax:   408-522-8041

"... because only those who write the code truly control the project."

------------------------------

Date: Thu, 23 Mar 2000 11:26:36 -0600 (CST)
From: Gary Kedziora <kedziora@chem.nwu.edu>
Subject: Re: OON: Comparison between Fortran & C++

Steve Karmesin writes:
> There is a semantic difference in passing arrays to subroutines in
> FORTRAN compared to C++: In FORTRAN you are guaranteeing that they are
> not aliased.  This can a really big deal for the optimizer when doing
> numeric intensive work, and is the primary remaining factor that makes
> many FORTRAN programs faster than the equivalent C++ programs.  There
> are proposals to improve this situation in C/C++ 
...

In principal C++ should be just as fast as FORTRAN (or just as 
good at optimizing), right?  Which C++ vendors are intersted in this?

I would like to understand Steve Karmesin's point quoted above better.
I've seen similar statements about aliasing in the OON literature
but have not seen an in-depth discussion or references to an in-depth
discussion.  Can someone point out some references?  

Why does C++ use this aliasing?  What is hard about removing it?


____
Gary Kedziora // Department of Chemistry // Northwestern University 
email: kedziora@chem.nwu.edu//ph: (847)467-4857//fax: (847)491-7713

------------------------------
Date: Thu, 23 Mar 2000 11:53:38 -0600
From: Jim Phillips <jim@ks.uiuc.edu>
Subject: Re: OON: Comparison between Fortran & C++

Geoffrey Furnish wrote:
> 
> One tiny point of clarification:
> 
> Steve Karmesin writes:
>  > There is a semantic difference in passing arrays to subroutines in
>  > FORTRAN compared to C++: In FORTRAN you are guaranteeing that they are
>  > not aliased.
> 
> Please someone correct me if what I say next is wrong, but I don't

> words, FORTRAN doesn't guarantee that arrays are unaliased, it assumes
> it.  You'd better make sure you don't violate that assumption, and yet
> htere are common programming styles which do so routinely.
I think Steve's comment should be interpreted as "In FORTRAN you (the
programmer)
are guaranteeing (to the compiler) that they (arrays) are not aliased
(and
hence the compiler will make this assumption when optimizing).

- -Jim

------------------------------

Date: Thu, 23 Mar 2000 11:24:00 -0700
From: ferrell@cpca.com
Subject: RE: OON: Comparison between Fortran & C++

Geoffrey Furnish writes:
 > One tiny point of clarification:
 > 
 > Steve Karmesin writes:

 >  > There is a semantic difference in passing arrays to subroutines in
 >  > FORTRAN compared to C++: In FORTRAN you are guaranteeing that they are
 >  > not aliased.  
 > 
 > Please someone correct me if what I say next is wrong, but I don't
 > think this is really the right way to say it.  My understanding is
 > that the language /assumes/ array parameters to subroutines are
 > unaliased, and allows the compiler to optimize accordingly.  

The FORTRAN standard is clear that aliasing is not allowed.  The
standard also does not allow implicit reshaping of arrays across
subroutine boundaries.  The way to alias data is through use of
pointers.  The way to reshape arrays in through intrinsics such as
RESHAPE.  Use of pointers inhibits some optimizations, as it should.
RESHAPE may or may not be efficient (e.g. logical reshaping, while
leaving the data unchanged in memory).

Like all other languages, users are not required to write standard

conforming code.  In general, the standard does not describe the
behavior of non-conforming programs.  I'm not sure what you are
driving at by saying that "FORTRAN doesn't guarantee that arrays are
unaliased, it assumes it".  The compiler doesn't guarantee it, but the
standard does.  The relevance to optimizing compilers is that FORTRAN
compilers may make assumptions about aliasing, just as they make other
assumptions about FORTRAN code.  What is lacking in C++ is a standard
way to tell the compiler that it's worries about aliasing are
unfounded.  Of course, use of such a pragma would not (need not) cause
the compiler to guarantee that arrays weren't aliased.  As always,
users are free to make mistakes and write non-conforming code.  Even
though FORTRAN users guarantee there is no aliasing, sometimes they
(unintentionally?) lie.

- -robert

------------------------------
Date: Thu, 23 Mar 2000 13:39:33 -0500 (EST)
From: jsiek@lsc.nd.edu
Subject: Re: OON: Comparison between Fortran & C++

To summarize, C++ is safe but slower (due to aliasing) and Fortran is
dangerous and fast. They are both sub-optimal. Neither language allows
the user to specify the aliasing behaviour, instead they make
assumptions. Something like C's restrict keyword is a step in the
right direction, however the restrict pointer is not a terribly good
match for modern C++ and generic libraries, since they don't deal with
pointers, but instead with the more abstract iterator. Last fall I
began working on a several different notions of how to abstract the
idea of a restrict pointer...  then things here at SGI got a little
crazy :(


Cheers,

Jeremy

P.S. Note that in C++ you can regain most of the lost performance by doing
some of the optimizations yourself (like loop unrolling), but that of
course is a pain.

------------------------------

Date: Thu, 23 Mar 2000 13:52:57 -0500 (EST)
From: jsiek@lsc.nd.edu
Subject: RE: OON: Comparison between Fortran & C++

A standard sanctioned assumption is still an assumption ;) Some
assumptions are worse than others... assumptions that can not be
checked at compile time are in the "worse" category.

ferrell@cpca.com writes:
 > The FORTRAN standard is clear that aliasing is not allowed.  The

 > standard also does not allow implicit reshaping of arrays across
 > subroutine boundaries.  The way to alias data is through use of
 > pointers.  The way to reshape arrays in through intrinsics such as
 > RESHAPE.  Use of pointers inhibits some optimizations, as it should.
 > RESHAPE may or may not be efficient (e.g. logical reshaping, while
 > leaving the data unchanged in memory).
 > 
 > Like all other languages, users are not required to write standard
 > conforming code.  In general, the standard does not describe the
 > behavior of non-conforming programs.  I'm not sure what you are
 > driving at by saying that "FORTRAN doesn't guarantee that arrays are
 > unaliased, it assumes it".  The compiler doesn't guarantee it, but the
 > standard does.  The relevance to optimizing compilers is that FORTRAN
 > compilers may make assumptions about aliasing, just as they make other
 > assumptions about FORTRAN code.  What is lacking in C++ is a standard
 > way to tell the compiler that it's worries about aliasing are
 > unfounded.  Of course, use of such a pragma would not (need not) cause
 > the compiler to guarantee that arrays weren't aliased.  As always,
 > users are free to make mistakes and write non-conforming code.  Even
 > though FORTRAN users guarantee there is no aliasing, sometimes they
 > (unintentionally?) lie.

------------------------------

Date: Thu, 23 Mar 2000 11:54:44 -0700
From: ferrell@cpca.com
Subject: Re: OON: Comparison between Fortran & C++

Gary Kedziora writes:
 > 
 > Steve Karmesin writes:
 > > There is a semantic difference in passing arrays to subroutines in
 > > FORTRAN compared to C++: In FORTRAN you are guaranteeing that they are
 > > not aliased.  This can a really big deal for the optimizer when doing
 > > numeric intensive work, and is the primary remaining factor that makes
 > > many FORTRAN programs faster than the equivalent C++ programs.  There
 > > are proposals to improve this situation in C/C++ 
 > ...
 > 
 > In principal C++ should be just as fast as FORTRAN (or just as 
 > good at optimizing), right?  Which C++ vendors are intersted in this?
 > 
 > I would like to understand Steve Karmesin's point quoted above better.
 > I've seen similar statements about aliasing in the OON literature
 > but have not seen an in-depth discussion or references to an in-depth
 > discussion.  Can someone point out some references?  
 > 
 > Why does C++ use this aliasing?  What is hard about removing it?

Aliasing shows up in ANSI C, and is not intrinsically associated with
OO programming.  It shows up because it is damn useful.  

Here's an example:

    #2          23-MAR-2000 14:48:56.77                                  NEWMAIL
        float a[100], *b;
        b = &a[1];
        do_the_nasty_math_thing(a,b,99)

   void do_the_nasty_math_thing(float *a, float *b, int n)
     {  int i;
        for (i=0;i<n;i++) {
            b[i] = b[i] + a[i];
        }
        return
     }

Given how b was assigned it would be quite incorrect of the compiler
to assume that the computation of iteration i is independent of the
computation of iteration i+1.  But an optimizing compiler quite likely
would like to do that.

The FORTRAN standard dictates that arguments which refer to the same
thing (e.g. &b[0] == &a[1]) may not be assigned in the subroutine.
Thus a FORTRAN compiler could optimize the for() loop, and it would be
a user error to call it with overlapping arguments, as I did above.
There is no similar restriction on C users, so certain optimizations
may not be performed by C (or C++) compilers.

- -robert

------------------------------

Date: Thu, 23 Mar 2000 19:24:19 GMT
From: Jacek Generowicz <jmg@ecs.soton.ac.uk>
Subject: Re: OON: Comparison between Fortran & C++

> Why does C++ use this aliasing?

Because it admits pointers. As soon as you have done that, you cannot
guarantee that any given chunk of data isn't being referred to by some
pointer or other, which has the potential to mess with those data.

> What is hard about removing it?

If you remove pointers from C, it would no longer be C (it would be much
like FORTRAN with different syntax) and fans of the language might
object . . .

Jacek

------------------------------

Date: Thu, 23 Mar 2000 12:54:44 -0700
From: ferrell@cpca.com
Subject: RE: OON: Comparison between Fortran & C++

jsiek@lsc.nd.edu writes:
 > 
 > A standard sanctioned assumption is still an assumption ;) Some
 > assumptions are worse than others... assumptions that can not be
 > checked at compile time are in the "worse" category.

FORTRAN does not make any assumptions about programs.  The standard
defines the behavior of conforming programs.  It does not define the
behavior for non-conforming programs (except for limited requirement
to identify syntax errors and a few other conformance requirements).
That is an important and deliberate feature of the language.  For
instance, some FORTRAN compilers have a flag which indicates that the
code to be compiled uses array aliasing.  Such a compiler is still
FORTRAN compliant- even though it will produce correct results for
non-compliant programs, it also produces correct results for
standard-conforming programs.

Thus, it is worth realizing that FORTRAN is different from languages
which do make assumptions about user code.  The standard is
deliberately permissive, for better and for worse.  That is very much
in accordance with standard mathematical terminology.  0/x is defined
for x/=0.  There is no assumption that x/=0.  Merely the expression is
not defined for that case.

- -robert

------------------------------

Date: Thu, 23 Mar 2000 15:01:55 -0500 (EST)
From: Todd Veldhuizen <tveldhui@extreme.indiana.edu>
Subject: Re: OON: Aliasing (was: Comparison between Fortran & C++)

> I would like to understand Steve Karmesin's point quoted above better.
> I've seen similar statements about aliasing in the OON literature
> but have not seen an in-depth discussion or references to an in-depth
> discussion.  Can someone point out some references?  
> 
> Why does C++ use this aliasing?  What is hard about removing it?

Oh boy!  An essay question!  :-)

I am still figuring aliasing out, so consider all of this material
under a "I may yet be ignorant"-type disclaimer.

Outline:

1.  What is aliasing
2.  Examples of optimizations which are hard/impossible because of
    aliasing
    (a) Reordering loads/stores
    (b) Loop-invariant code motion/PRE
    (c) Constant/copy propagation through the store
3.  Remedies
    (a) Alias analysis
    (b) Command-line options
    (c) restrict
    (d) valarray
    (e) Hardware solutions


1. What is aliasing

Variables are aliases if they refer to overlapping storage 
locations.  A simple example is:

int x = 0;
int* p = &x;
*p = 10;
int y = x + 1;

Both x and *p refer to the same storage location.  
A naive compiler which did constant propagation without worrying
about aliasing would see 

int x = 0;

int y = x + 1;

and wrongly conclude that y == 1.  

Aliasing is not unique to C++: it is also a problem in languages such
as Java, Scheme, Fortran 90.  Fortran 77 is actually the oddball.

Any compiler which wants to optimize C++ has to worry about aliasing.
There are two types of aliasing information compilers gather:

  may-alias:  x and *p might refer to the same storage location(s)
  must-alias: x and *p definitely DO refer to the same storage location(s)

Performance problems are caused by may-alias information.  If the
compiler cannot prove that pointers are not aliased, then it has to
assume the worst.  Must-alias information is needed if the compiler
wants to do e.g. constant/copy propagation through aliased pointers.
Looking at the above example, with may-alias information the compiler
would say, x and *p might alias.  I don't know for certain that they
DO alias, so the assignment to *p invalidates anything I might know
about x.  The code after optimization would be:

int x = 0;
int* p = &x;
*p = 10;
int y = x + 1;

i.e. no changes, because the compiler doesn't know whether x and p
actually alias.

With must-alias information, the compiler would say, x and *p definitely
DO refer to the same store location.  Then it can do constant
propagation despite the aliasing:

int x = 0;

int* p = &x;
*p = 10;
int y = 11;


2.  Examples of optimizations which are hard/impossible because of
    aliasing

Here are some examples of optimizations which are difficult/impossible
due to possible aliasing:


(a) Reordering loads/stores

Compilers get good performance on numerical kernels by "software pipelining".
The basic idea is exploit parallelism by partially unrolling the inner
loop.  An example:
      void daxpy(int n, double a, double* x, double* y)
      {
          for (int i=0; i < n; ++i)
            y[i] = y[i] + a * x[i];
      }

After unrolling this 4 times we get:

      void daxpy(int n, double a, double* x, double* y)
      {
          for (int i=0; i < n; i += 4)
          {
            y[i]   = y[i]   + a * x[i];
            y[i+1] = y[i+1] + a * x[i+1];
            y[i+2] = y[i+2] + a * x[i+2];
            y[i+3] = y[i+3] + a * x[i+3];
          }
          // cleanup code for (n % 4) != 0 here
      }

If there is no aliasing between x and y, the compiler can
rearrange the loads and stores to get a good schedule.  Here's
a hand-faked version to give the idea:

      // fake pipelined DAXPY loop
      double* xt = x;
      double* yt = y;
      for (; i < n; i += 4)  
      {
          double t0 = yt[0] + a * xt[0];
          double t1 = yt[1] + a * xt[1];
          yt[0] = t0;        
          double t2 = yt[2] + a * xt[2];
          yt[1] = t1;       
          double t3 = yt[3] + a * xt[3];
         i += 4;
          xt += 4;
          yt[2] = t2;
          yt += 4;
          yt[3] = t3;
      }

This version overlaps the load/stores with floating-point computations,
doing the same amount of work in fewer clock cycles.

BUT: if x and y might alias, then the compiler cannot rearrange the
loads and stores, because writing y[0] could change the value of
x[1] or x[2] or x[3].  

In C++ the compiler has to assume that x and y might alias.  Hence
the loads and stores cannot be rearranged.

The resulting performance loss is primarily a problem for in-cache
problems: you can see a loss of 20-50%.  Aliasing tends to be less
of a problem for out-of-cache problems: memory latency tends to
overwhelm the benefits of software pipelining.


(b) Partial redundancy elimination

Partial redundancy elimination combines common subexpression elimination,
loop-invariant code motion and other stuff.

Here's a typical application: suppose we want to calculate the
rank-1 update of a matrix:  A <- A + x x'    (where x' = x transpose)
We could code this in C++ as:

      void rank1Update(Matrix& A, const Vector& x)
      {
          for (int i=0; i < A.rows(); ++i)
            for (int j=0; j < A.cols(); ++j)

              A(i,j) += x(i) * x(j);
      }

For good performance, the compiler needs to keep x(i) in a register
instead of reloading it constantly in the inner loop. However, the
compiler can't be sure that the data in the matrix A doesn't overlap
the data in the vector x -- there is a possibility that writing to
A(i,j) could change the value of x(i) partway through. This might be
impossible given the design of the Matrix and Vector classes, but the
compiler doesn't know that. The remote possibility of aliasing forces the
compiler to generate inefficient code.


(c) Constant and copy propagation through the store (heap/stack)

In C++ we often have lots of objects kicking around.  This leads
to the well-known "abstraction penalty": if you work with objects
(Iterator, Complex) instead of scalars (double) and pointers (double*), 
performance suffers.  Getting rid of this "abstraction penalty"
requires doing constant and copy propagation through the store,
to e.g. convert Iterators back into pointers, and eliminate the
temporary objects.

Analyzing the store is greatly complicated by aliasing.  Everytime you
want to propagate something out of the store, you have to ask: could
this region of the store have been modified through an aliased
pointer?  Far too often the answer is yes, and you can't do any
optimization.  For example:

void f(int* x)
{
  x[0] = 5;
  foo();
  y = 3.0 * x[0];
}


Can I assume that y = 15.0?
Well, that depends on what happens in foo, and whether
it could have access to the store locations pointed to by x.  

3.  Remedies

(a) Alias analysis

In alias analysis, compilers automatically determine may-alias
and/or must-alias information.

Some forms of aliasing are ruled out by the ANSI C standard.
Type-based aliasing rules stipulate (for example) that float* and
double* pointers will not alias.

Alias analysis is useful but not perfect:
- - precise alias analysis is NP-hard.  This is true even for
  the crudest form of alias analysis, namely flow-insensitive local

  alias analysis (i.e. not interprocedural).  (See [3] below).  So
  alias analysis is ALWAYS going to be approximate, not precise.
- - ruling out aliasing of function arguments requires interprocedural,
  closed program analysis.  i.e. it is NOT compatible with the separate
  compilation model followed by most C++ compilers.  (for example, to
  solve the daxpy(..) aliasing problem describe above, one would have
  to analyze a complete program and prove that there are no calls to
  daxpy(..) for which x and y alias.  This requires being able to
  examine the whole program at once.)  Some compilers e.g. SGI will
  do interprocedural alias analysis (see -IPA options).
- - pointers frequently "escape" to where the analysis cannot follow.
  Example: Invoking virtual functions, function pointers, or external
  functions requires making a worst-case assumption about aliasing.
  Also, in C++ you have to make aliasing decisions like: does x.y.w
  alias w.e.f[4].g?  Putting things into containers (e.g. list<T>) and
  pulling them out also kills any useful alias information you might
  have had about a particular T.
(b) Command-line options

Some compilers have options like "assume no aliasing of function
parameters" which duplicate the behaviour of Fortran no-aliasing
restrictions.

These can really blow up (as Geoffrey Furnish described) if your code
happens to rely on aliasing being handled correctly.


(c) restrict

NCEG (Numerical C Extensions Group) designed a keyword restrict which
is recognized by some C++ compilers (KAI, Cray, SGi, Intel, gcc).
See: http://www.lysator.liu.se/c/restrict.html
This has been accepted into the C9x standard, so the chances of it
appearing in the next C++ standard are good, assuming people can
agree on what it means in C++.

restrict is a modifier which tells the compiler there are no aliases
for a store location.  For example:

  double* restrict a;    

This means that there are no aliases for any store locations
accessed through a.  Hence the compiler is free to rearrange
load/store orderings and do other optimizations.

Unfortunately restrict does not extend well to C++ idioms such
as iterators.  For example, I cannot say:

Iterator restrict iter = x.begin();

to mean that the data referred to by iter is not aliased by
any other iterators.  Instead I would have to choose between:
  (a) Making the pointer inside Iterator "restrict", meaning
  that I can never use two Iterators which refer to the same
  container simultaneously;
  (b) Providing two versions of Iterator, one of which is
  alias-free and one of which is not, for example:
  Iterator and AliasFreeIterator.

Neither of these options are very appealing.
There is also some weirdness around restrict not being an "official"
part of the type.  So Vector<double*> and Vector<double* restrict>
would be the same template instance, as far as I understand.  I
guess this would get sorted out by the standards committee.

(d) valarray

Restrict never made it into the ISO/ANSI C++ standard.  There is
however this statement about valarray:

  2 The valarray array classes are defined to be free of
    certain forms  of aliasing, thus allowing operations

I don't know of any compiler which makes use of this information.

(e) Hardware solutions

The new IA-64 processor architecture has "data speculative loads"
which may solve aliasing performance problems in hardware.  The
idea is to allow loads to be optimistically moved before stores,
despite possible aliasing.  The processor determines on the fly if a
subsequent store invalidates the load, and cancels it.

Here's an example from the specs.  Code before optimization:

st8  [r4] = r12         // Store contents of r12 at location [r4]
ld8  r6 = [r8];;        // Load from location [r8] to r6
add  r5 = r6, r7;;
st8  [r18] = r5         // Store r5 at [r18]
IA-64's "data speculation" lets you move the load before the store.
The load is split into two instructions: ld8.a, an "advanced load", and
"ld8.c.clr" the "check load + clear":

ld8.a  r6 = [r8];;      // advanced load
st8    [r4] = r12
ld8.c.clr r6 = [r8]     // check load
add    r5 = r6, r7;;
st8    [r18] = r5

The load starts when the ld8.a instruction is reached.  When the check
instruction ld8.c.clr is reached, if the store clobbered the advanced
load, then the load is done a second time.  Otherwise it is a nop.
The IA-64 has a special hardware table where it checks stores against
advanced loads.

So it might solve the problem we have now of software pipelining
loops in the presence of aliasing.  If there is aliasing, your code
will "magically" run slower because of the check loads failing and
re-issuing the loads.  If there isn't aliasing, the check loads
will be no-ops.  I don't know whether the check loads introduce
latency or muck up the scheduling.


More info:
[1] Advanced Compiler Design & Implementation, Steven S. Muchnick, Chapter 10
    (Alias Analysis)
[2] NCEG restrict proposal: http://www.lysator.liu.se/c/restrict.html
[3] Precise Flow-Insensitive May-Alias Analysis is NP-Hard, Susan Horwitz
    http://www.acm.org/pubs/citations/journals/toplas/1997-19-1/p1-horwitz/
[4] restrict in C9x
    http://www.accu.org/events/public/c_news.htm
[5] IA-64 Application Developer's Architecture Guide
    Section 4.4.5: Data Speculation
    http://developer.intel.com/design/ia-64/downloads/adag.htm

- -- 
Todd Veldhuizen               tveldhui@acm.org
Indiana Univ. Comp. Sci.      http://extreme.indiana.edu/~tveldhui/

------------------------------

End of oon-digest V1 #57
************************

--------------------- Object Oriented Numerics List --------------------------
* To subscribe/unsubscribe: use the handy web form at
http://oonumerics.org/oon/
* If this doesn't work, please send a note to owner-oon-list@oonumerics.org




