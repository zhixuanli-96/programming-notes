\documentclass[a4paper]{article}
\usepackage{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{color}

% \definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstset{ language = C++, basicstyle = \footnotesize, numbers = left,
  backgroundcolor = \color{backcolour},
  keywordstyle = \color{magenta},
  commentstyle = \color{codegreen},
  stringstyle = \color{codepurple}, 
  showstringspaces = false
}

\newcommand{\rmd}{\mathrm{d}}

\begin{document}

\title{A Short Introduction to C++ for Math Majors}
%\author{Zhixuan Li}
\date{}
\maketitle

\setcounter{tocdepth}{1}
\tableofcontents
\newpage

\section{The Goal of This Lecture}

This is a note complementary to
the theoretical part of the Algebraic Topology course. 
As has been emphasized,
profound theories and practical applications are closely interwined.
In this course, you are trained to tackle real-world problems
using the power of mathematical theories.
For mathematics researchers,
C++ programming turns out to be
a great tool for realizing those theories
and putting them to practical use.

I will try my best to guide you into the world of C++,
with emphasis on the connections between mathematics and programming.
Of course, it takes time and practice to
master the language and
become skilled at applying theories.

From the practical view point, you will need the
knowledges and skills presented in this note
in your programming projects. 

\section{Object-Oriented Programming}

The core of Object-Oriented Programming (OOP)
is to base your code on the concepts of objects.

\begin{itemize}
\item \textit{Classes. } The blueprint of a kind of objects, 
  including data format and possibly procedures that apply on them.
  $C^{\infty}([0,1])$ for example.
\item \textit{Objects. } An object is an instance of a class. 
  A class can have multiple instances,
  just like you can build many houses out of a blueprint.
  $f \equiv 1$ is an object (or you may say an element) of $C^{\infty}([0,1])$. 
\item \textit{Methods. } Operations that you can perform on objects.
  Each class has its own methods. 
  $\rmd / {\rmd t}$ can be applied to any object of $C^{\infty}([0,1])$,
  for example. 
\item \textit{Data members. } Properties that belong to every object,
  yet the specified values vary from objects to objects.
  For every $f \in C^{\infty}([0,1])$, $f(0.5)$ is a `property' of $f$.
  For different $f, g \in C^{\infty}([0,1])$,
  $f(0.5)$ and $g(0.5)$ may have different values. 
\end{itemize}

The object-orientation point of view is beneficial
in the following ways :
\begin{itemize}
\item \textit{Encapsulation. }
  Class users interact with objects through class methods
  without worrying about the implementation details.
  Class designers focus their work on writing implementations
  while user codes are not disturbed.

\item \textit{Reusability. } Existing codes can be reused and
  left untouched through inheritance.
\end{itemize}

An important rule in OOP is that
\underline{one class should only take one reponsibility}.
In applications with strong math background,
one class corresponds to one mathematical concept.
Also, components of a class should work harmoniously
and contribute to an organic whole.
Grouping unrelated data and methods into a class
does not align with OOP.

\subsection{Synopsis}

$\bullet$ Declaration of class. Access specifiers. 
$\bullet$ Constructors and destructors. Initializers. 
$\bullet$ Declaration of member functions and data members.

\lstinputlisting{src/1.cpp}

\section{Inheritance and Composition}

\begin{itemize}
\item Example of inheritance :
  group$\leftarrow$abelian group$\leftarrow$cyclic group.
  group$\leftarrow$ring$\leftarrow$integral domain$\leftarrow$field.
  
  Inheritance implies an is-a relationship.
  You should not inherit B from A
  if you are not sure whether B is an A.

\item Do cancellation laws hold in a field?
  Yes they do, because a field is an integral domain
  and an integral domain is so defined.
  (If B is an A, everything we know about A applies to B!)

\item Example of composition : abelian group + scalar multiplication = vector space.
  
What is the difference between inheritance and composition ?
%  // vector space over Z is a special abelian group. 

\item To prove every vector space has a set of basis, you will need to make use of both defining components of a vector space.
  (If A consists of B and C, to let A get the job done, you can
  (i) have B alone get the job done,
  (ii) have C alone get the job done,
  (iii) more importantly, let B and C co-operate! )

\end{itemize}

\subsection{Synopsis}

$\bullet$ Declaration of derived class. 
$\bullet$ Access specifiers, visibility of members
\footnote{https://stackoverflow.com/questions/860339/difference-between-private-public-and-protected-inheritance}
.
$\bullet$ Initializer lists.

\lstinputlisting{src/8.cpp}

\underline{Caveat} :
There are possibly dependencies between data members.
The order of initializer lists should match that in member declaration
\footnote{
  https://stackoverflow.com/questions/1242830/constructor-initialization-list-evaluation-order
}.
%(The book gives a wrong example. )


\subsection{Discussions}

Opinions on multiple inheritance
\footnote{
  https://stackoverflow.com/questions/406081/why-should-i-avoid-multiple-inheritance-in-c}
.

\section{Practice : Compile and Run Your C++ Program}

\section{Polymorphism}

Suppose you are given some space $X$,
a sequence $\{x_n\}_{n=1}^{\infty} \subset X$ and a point $x^{\ast} \in X$,
and you are asked to check if $x_n \rightarrow x^{\ast}$.

Although the behavior of convergence is well-defined
provided $X$ has a topology,
the judging criterion for a specific space varies greatly
(due to their varying topologies).

\begin{itemize}
\item If $(X,\mathcal{T})$ is a topological space,
  you should check that every neighbourhood of $x^{\ast}$ contains
  all but finitely many points in $\{x_n\}$.
\item For $(L^{\infty}(\Omega), \Vert \cdot \Vert_{\infty})$,
  check that for each $\epsilon > 0$
  there exsits an $N \in \mathbb{N}^+$
  and a collection of zero-measure sets $\{U_n : U_n \subset \Omega\}$,
  such that $\vert x_n(\cdot) - x^{\ast}(\cdot) \vert < \epsilon$
  on $\Omega \setminus U_n$ whenever $n \ge N$.
  (Recall essup.)
\item For the simple $(\mathbb{R}, \vert \cdot \vert)$,
  you should follow the standard argument : 
  Check for each $\epsilon > 0$, there exists an $N \in \mathbb{N}^+$
  s.t. $\vert x_n - x \vert < \epsilon$ whenever $n \ge N$.
\end{itemize}

The core of polymorphism is that,
your reaction to the same instruction may vary
depending on the objects you are working on. 

\subsection{Synopsis}

$\bullet$ Virtual member functions.
$\bullet$ Virtual dtor. \underline{What's the order of the dtors get invoked}?
$\bullet$ (Optional) Abstract base class. 

\lstinputlisting{src/2.cpp}

\subsection{Casting}

\begin{itemize}
\item Upcast.
  Converting a derived class object to a base class object
  \textit{by pointer or reference} is always valid.
  Polymorphism comes into effect
  when calling a virtual function through a base class pointer/reference. 
\item Downcast.
  Compilers will complain about a downcasting.
  If you have to frequently perform downcasting in your code,
  you should consider re-organizing the hierarchy of classes. 
\item (Optional) Object-slicing
  \footnote{https://stackoverflow.com/questions/274626/what-is-object-slicing}
  occurs when converting a derived class object
  to a base class object \textit{by value}.
  Polymorphism won't take effect.
\end{itemize}

\subsection{Discussions}

What's the difference among redefinition, overloading and polymorphism?

\begin{table}[htb]
  \centering
  \begin{tabular}{llll}
    & Redefinition & Overloading & Polymorphism \\
    \hline
    Hide base class function? & & & \\
    Within a class or within a hierarchy? & & & \\
    Allow signature change? & & & \\
    When does it take effect?  & & &
  \end{tabular}
\end{table}

\section{Template Programming}

It is sometimes beneficial to work on symbols,
rather than on concrete objects. 
For instance, when studying the ring of polynomial $K[x]$ on a field $K$,
we use the symbol $x$ to denote an arbitrary element.
If we manage to factorize a $P(x) \in K[x]$,
then the factorization remains valid
with $x$ replaced by a scalar or even a matrix. 

The typical steps we work with symbols are :
\begin{itemize}
\item Extract universal patterns that appear across different types.
\item Summarize them in symbolized form.
\item Make substitutions to recover a specialized object, or a result. 
\end{itemize}
In such a way, generality, clarity and conciseness are gained. 

In C++ template programming, writing a class template is to create a pattern,
while instantiating a class template is to make substitutions. 

\subsection{Synopsis}

\lstinputlisting{src/3.cpp}

More discussions on templates in future sections. 

\section{STL and Iterators}

This section is purely from the practical point of view.
There are conventions you need to follow in order to
make efficient use of C++.

\subsection{STL}

Standard Template Library (STL) is a collection of
class templates and algorithms
released with standard C++.

The frequently-used data structures such as
(dynamic-sized) array, list, stack and queue
have their implementations in STL,
i.e. std::vector$<>$, std::list$<>$, std::stack$<>$, std::queue$<>$ and so on.
(The $<>$ indicates they are class templates, not classes. )

Each kind of containers is also equipped with
common algorithms such as searching, sorting, insertion and deletion etc.

You should always make use of STL containers or algorithms if possible. 
Doing so adds to the efficiency, and most importantly,
readability and friendliness of your code.

\subsection{Iterators}

An array is a linear container while a tree is not.
It means that we need to write separate codes
in order to traverse them.
From the users' point of view, however,
implementations are not their concerns. 
All they need is an mechanism that
gives access to each element in the container
once and only once. 
Iterators are such an mechanism,
and this explains why
iterators are regarded as a good design
of separating users from implementation details.

\lstinputlisting{src/4.cpp}

\section{Practice : Automation and the Make System}

\section{Styles and Techniques}

In this section we explore some techniques that make your code clean and reusable. 

Functional programming is among those techniques,
without an intuitive definition though
(you can check the Wikipedia version).
Roughly speaking,
you write a self-contained sub-program
(usually called lambda in python and C++, or closure in other languages)
and pass it as an argument to another driver program.
The driver program co-operates with the sub-program,
treating it like a black-box,
to produce further results.
On the software engineering side,
functional programming lowers the programming complexity
and often leads to better reusability of codes. 

\subsection{Sorting with Multiple Keywords}

Mathmatically,
a partial order on a set $S$ completely defines
the outcome of sorting a subset of $S$.
The game does not change in C++ : 
it is our freedom of choice to feed std::sort with any partial order.
Note how the partial order is prescribed by an auxilary function. 

\lstinputlisting{src/7.cpp}

\subsection{Recursion}

In the shortest version,
recursion is the behaviour that
the definition of one object involves itself.
In programming context,
recursion is narrowed to the behaviour
that a function calls itself. 
Recursion is such a natural pattern and so fit into our brain
that it appears in every aspect of math and in every serious piece of code. 
Thinking in a recursive manner usually makes your code clean
and less vulnerable to logical bugs. 

\begin{itemize}
\item The recursive definition of a tree.
%\item Mathematical induction. 
\item The definition of the Cantor set, the Koch snowflake, etc.
\item In computational methematics, the Multigrid method for solving Poisson equation
  recursively sends smooth errors onto coarser grid.
\item The Depth-First-Search (DFS) algorithm to traverse a graph. 
\end{itemize}

\lstinputlisting{src/6.cpp}

Exercise : Suppose you are to climb a staircase.
You can choose to climb up 1 step or 2 steps each time.
How many different ways are there to climb a staircase of $N\ge3$ steps ?

\underline{Caveat} : To avoid an inifinite recursion,
you have to explicitly prescribe a stopping criterion.

\subsection{Template Technique : Compile-Time Constant}

We conclude this section with an old-fashioned example
that employs the technique of compile-time programming.
(\underline{What is the difference between compile-time and run-time}? )

\lstinputlisting{src/5.cpp}

\section{Practice : Debugging Your Program}

%\section{Closing Words}

% \section{(Optional) A Glimpse into Modern C++}

% Like any other widely spoken language,
% C++ evolves.
% The term \textit{modern C++}
% refers to a set of new language features,
% known as C++11/14. 
% % (released in 2011 and 2014 respectively, of course).
% The point here is not to talk in-depth about the rich new features,
% but to persuade you that coding life has become much easier
% thanks to the evolution of language.

% \subsection{Lambda Expressions}

% The introduction of lambdas in C++
% actively encourages the style of functional programming. 

% \subsection{Factorial Revisited}

% If compile-time factorial is exactly what you want,
% the following is standard in modern C++. 

% \subsection{The ranged-for Syntax}

% Thankfully the \textit{ranged-for} syntax saves you a lot of typing
% (but this is not the only gain). 

% \subsection{To Write Code Is To Write Math}

% On top of a one-dimensional numerical quadrature,
% how do you build the higher-dimensional ones?

\section{Resources}

\begin{itemize}
\item \textit{Thinking in C++} by Bruce Eckel.
  
  This is an introduction to the language covering a variety of topics.
  A book of authority. For beginners.

\item \textit{Modern C++ Design: Generic Programming and Design Patterns Applied}
  by Andrei Alexandrescu.
  
\item \textit{The C++ Programming Language} by Bjarne Stroustrup.
  
  A book by the creator of C++.
  
\item \textit{cppreference.com}

  A reference to the standards of the language and the standard libraries.
  It is convenient and well-indexed. Available in offline html format.
  Strongly recommended for programmers of all levels.

\item \textit{stackexchange.com}

  This is an such amazing forum that
  (i) When the compiler throws you an absurd error message,
  you are likely to find an explanation and solution on stackexchange!
  (ii) You can also find explanatory posts
  regarding concepts, designs, and language features
  shared by experts from all over the world. 

\item \textit{Modern Effective C++ by Scott Meyers}

  A guide for transitioning from old C++ to modern C++. For skilled programmers.

\item \textit{ZOJ (acm.zju.edu.cn) and POJ (poj.org)}

  For who wants to improve your ability in designing algorithms.
  Full of problems and challenges (less related to mathematics though).
  
\end{itemize}

% \section{Softwares}

% \begin{table}[htb]
%   \centering
%   \begin{tabular}{ll}
%     \hline
%     OS & Ubuntu 16.04 LTS or later \\
%     Compiler & gcc \\
%     Debugger & gdb \\
%     Text Editor & Emacs \\
%     \hline
%   \end{tabular}
% \end{table}

\end{document}